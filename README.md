# Slack Infrastructure Engineering: Technical Exercise Prompt

## How to Run

    npm install -g typings
    npm install -g gulp
    npm install && typings install

    gulp test
    gulp compile
    
## Overview

    ├── binary_format.ts                 # handle binary parsing and packing
    ├── binary_format_test.ts            # unit test
    ├── config.js                        # configuration for resources
    ├── defs.ts                          # enums and headers
    ├── helper.ts                        # utility to construct binary packet from text
    ├── linked_buffer_list.ts            #  
    ├── linked_list.ts                   # data structure used by LRU
    ├── lru_cache.ts                     # Implementation of LRU, including GC
    ├── lru_cache_test.ts                # unit test
    ├── server.ts                        # TCP server exposes the streams
    ├── stream_transformer.ts            # binary streaming parser and LRU as streaming service
    └── stream_transformer_test.ts       # unit test

## This exercise is meant to demonstrate:
An understanding of servers, networking, and protocols.

An understanding of concurrency, performance, and resource constraints, and an
ability to anticipate future issues and implement solutions.

An ability to write clear, easy to understand code, communicate your approach,
and reason about tradeoffs that you have made.

## The exercise:
Write a memcache server that has support for the `Get`, `Set`, and `Delete`
methods using either
the
[Memcache Text Protocol](https://raw.githubusercontent.com/memcached/memcached/master/doc/protocol.txt) or
the
[Memcache Binary Protocol](https://cloud.github.com/downloads/memcached/memcached/protocol-binary.txt). *Include support for Compare And Swap*, either by implementing the GETS and CAS
methods in the text protocol or by using the CAS field in the binary protocol.
You don't need to implement support for the expiry fields.

Support a configurable limit for the resource consumption of your service.

Handle concurrency issues that arise in an environment with multiple
simultaneous writers and readers.

## Discussion:

### [Extra] Language & Runtime Choices

I evaluated the problem and chose Typescript + nodejs for the following reason:

- Concurrency is a major requirement for this service. The nodeJS runtime
  simplifies the problem by only executing the program in a single-thread. No
  racing condition would happen in place expect IO. Having one socket per TCP

  client connection obviates possible racing condition in IO.

- The Stream interface fits the problem domain well. The service could be
  thought as a binary bytes transformer. Meanwhile, stream makes inject
  middleware such as logger easily. Streaming the data buffer to memory or
  client also reduce the memory usage (not needs to hold the whole buffer in
  memory for #get/#set), increase the throughput and reduce latency.

- JavaScript has native Buffer object to manipulate bytes and `number` is 64 bit
  which could hold every field of head (especially the `cas`).

- Types are great to express interface, data structure and make program more
  robust: all good for development at large scale. I grappled with Closure
  Compiler in order to make it work with nodejs; failed and just switched to


- I am fluent in ES5/ES6, so it helps with productivity. (Learning TypeScript
  takes 1-2 hours).

### Tell us what you expect the performance of your implementation to be, and where you anticipate bottlenecks.

TODO: benchmark.ts

Bottleneck will be LRU implementation. Current implementation used a linkedlist,
which has O(n) time complexity where n is the number of items. This might
experience increase of latency when items are relative small in bytes and number
of items are large.

### Suggest ways in which the performance, reliability, or other limitations of your implementation could be improved upon in the future.

Performance:
- The internal data storage is using LinkedList, this has O(n) time complexity
  for search/delete where n is the number of items. This affects all public
  functionality - get/set/delete. A more complex solution such as using two
  binary search tree, one for cas, one for key string could reduce the time
  complexity to O(lgn)
- The reading and writing of data buffer happens in memory, this could be
  refactored to leverage the current streaming implementation. Thus for #get
  response, the buffer read from data storage would be streamed onto the TCP
  socket; vice verse for a #set request.
  
Reliability:
- The handling of TCP binary stream should be more robust to handle malformed
  binary stream or invalid binary format etc.
  

Other limitation:
- ???

### Suggest or implement ways in which your service could be monitored and managed in a production setting.

#### What to Log

- Resource. 
  - General process's information: uid, memory, cpu, uptime, TCP connections, fds,
  - Application information such as storage's bytes, current number of elements,
    accumulated number of items, next cas number.

- Traffic. The accumulated number of every type of requests received (response
  sent) or numbers per timeframe (last 60/300/1800 seconds etc.)

- Effectiveness. Number of errors, cache hit/miss for each type of requests.


#### How to Log

- For Resource. Logs could be either accumulated in memory (if small) or append
  to disk files. For example, the storage's status and the accumulated number of
  requests could be kept in memory and periodically write to disk. 

- For Traffic. As all traffic is abstracted as stream, one or multiple loggers
  could be inserted in the stream pipeline to collect different layers of
  information. For example:

        socket
            .pipe(new RawBinaryLogger())   // <--------- This would log binary request information
            .pipe(new RequestParser())
            .pipe(new RequestLogger())   //  <--------- This logs request to memcache server
            .pipe(new MemcacheServer(cache))
            .pipe(new ResponseLogger())   //  <--------- This logs response from the memcache server
            .pipe(socket);
            
  Each logger would write logs to disk.
            
#### How to Expose Log

The log could be exposed through another command or through a separate HTTP
server. Which collects all the log information either from memory or from disk
and send to the log request client.

### Show how you tested your server.

- The major components are unit tested: `binary_format_test.ts` covers the binary parser / packer; `lru_cache_test.ts ` covers logics of implemented operation (get/set/delete), cas and LRU algorithm.
- The server itself is covered with e2e test `e2e_test.ts`.


### Provide instructions on what we need to do to build and run your server locally on a standard unix-like development environment (e.g. Mac laptop or Linux server).


## Notes:
You should choose an appropriate language and runtime given the requirements of
the exercise. Feel free to run your choice by us if you have questions.

You may use basic collections and support classes from the language of your
choice, but you are responsible for implementing the resource constraints,
eviction algorithm, and concurrency control.

We don't expect you to reimplement all of memcache in a few hours. (Please don't
try to!) It's more important that your solution work and be complete, and that
you can reason about the performance tradeoffs and potential concurrency issues
that you'd expect given real-world usage.

Please provide all source code via a Git repository – create a local repo for
your code, and when the exercise is complete, please tar or zip your main
directory and send it along to us (and please remember to include your .git
directory/files). If you’re not comfortable with Git, you may simply provide a
tgz of the source code.

Feel free to ask us any questions!
