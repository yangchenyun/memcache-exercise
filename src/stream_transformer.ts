/**
 * @fileoverview
 *
 * Expose the memcache as steam server.
 */
import stream = require('stream');

import {
    LruCache,
    CacheError
} from './lru_cache';
import {
    parseRequest,
    packRequest,
    parseResponse,
    packResponse,
} from './binary_format';
import { MagicKey, OpKey, StatusKey, keyToBytes } from './defs';
import {EMPTY_CAS} from './helper';


/**
 * RequestParser states.
 */
enum ParserState {
    HEAD,
    KEY,
    BODY,
};

const headerBytesLen = 24;

// NOTE: Parse TCP steam, header + data
// Handle parsing error, unchunk here.
export class RequestParser extends stream.Transform {
    private _buffer: Buffer;
    // The current state of parser.
    private _state: ParserState;
    // The array of Buffers which count towards the next buffer to process
    private _buffers: Buffer[];
    // Bytes left to process the buffers
    private _bytesLeft: number;
    // Packet in construction
    private _packet: any;

    constructor() {
        super({ readableObjectMode: true });
        this._setHeadState();
    }

    _setBodyState(packet: any) {
        this._bytesLeft = packet.bodyLen - packet.keyLen;
        this._state = ParserState.BODY;
        this._buffers = [];
        this._packet = packet;
    }

    _setKeyState(packet: any) {
        this._bytesLeft = packet.keyLen;
        this._state = ParserState.KEY;
        this._buffers = [];
        this._packet = packet;
    }

    _setHeadState() {
        this._bytesLeft = headerBytesLen;
        this._state = ParserState.HEAD;
        this._buffers = [];
        this._packet = null;
    }

    _pushPacket() {
        this.push(this._packet);
        this._setHeadState();
    }

    _process(chunk: Buffer, co?: Function) {
        this._buffers.push(chunk);
        this._bytesLeft -= chunk.length;

        if (this._bytesLeft < 0) {
            throw Error('Read more bytes than needed.');
        }

        if (this._bytesLeft > 0) {
            return;
        }

        // Ready to processing the buffer
        const buffer = Buffer.concat(this._buffers);
        if (this._state == ParserState.HEAD) {
            const packet = parseRequest(buffer);
            this._setKeyState(packet);
        } else if (this._state == ParserState.KEY) {
            this._packet.key = buffer.toString('ascii');
            if (this._packet.bodyLen > this._packet.keyLen) {
                this._setBodyState(this._packet);
            } else {
                this._pushPacket();
            }
        } else if (this._state == ParserState.BODY) {
            this._packet.buffer = buffer;
            this._pushPacket();
        }

        // Continuation
        if (co) { co();}
    }

    _data(chunk: Buffer, finished: Function) {
        if (chunk.length <= this._bytesLeft) {
            this._process(chunk);
            finished();
        } else {
            const chunkedBuffer = chunk.slice(0, this._bytesLeft);
            this._process(chunkedBuffer, () => {
                this._data(chunk.slice(chunkedBuffer.length), finished);
            });
        }
    }

    _transform(chunk: Buffer, encoding: string, callback: Function): void {
        this._data(chunk, callback);
    }
};


// Expose the memcache as a stream service
export class MemcacheServer extends stream.Transform {
    cache_: LruCache;

    constructor(cache: LruCache) {
        super({
            writableObjectMode: true
        });
        this.cache_ = cache;
    }

    // initialize the response for request
    _initRes(req: any, status: StatusKey) {
        return {
            magic: <MagicKey> MagicKey.RESPONSE,
            optCode: req.optCode,
            keyLen: 0,
            extrasLen: 0,
            dataType: 0,
            status: <StatusKey> status,
            bodyLen: 0,
            opaque: 0,
            cas: req.cas
        };
    }

    _pushBinary(response: any, buffer?: Buffer) {
        this.push(packRequest(response));
        if (buffer) {
            this.push(buffer);
        }
    }

    _get(request: any) {
        const res = this._initRes(request, StatusKey.NO_ERROR);
        const cell = this.cache_.get(request.key);
        res.cas = cell.cas;
        this._pushBinary(res, cell.buffer);
    }

    _set(request: any) {
        const res = this._initRes(request, StatusKey.NO_ERROR);
        const cell = this.cache_.set(request.key, request.buffer);
        this._pushBinary(res);
    }

    _cas(request: any) {
        const res = this._initRes(request, StatusKey.NO_ERROR);
        const cell = this.cache_.cas(request.key, request.buffer, request.cas);
        this._pushBinary(res);
    }

    _delete(request: any) {
        const res = this._initRes(request, StatusKey.NO_ERROR);
        this.cache_.delete(request.buffer.toString());
        this._pushBinary(res);
    }

    _transform(request: any, encoding: string, callback: Function): void {
        let res: any;
        if (request.magic != MagicKey.REQUEST) {
            res = this._initRes(request, StatusKey.INVALID_ARGUMENTS);
            this.push(packResponse(res));
            return;
        }

        if (request.optCode in [OpKey.GET, OpKey.SET, OpKey.DELETE]) {

            this.push(packResponse(res));
            return;
        }

        try {
            if (request.optCode == OpKey.GET) {
                this._get(request);
            } else if (request.optCode == OpKey.SET) {
                if (request.cas == EMPTY_CAS) {
                    this._set(request);
                } else {
                    this._cas(request);
                }

            } else if (request.optCode == OpKey.DELETE) {
                this._delete(request);
            }
        } catch (e) {
            if (e instanceof CacheError) {
                res.status = e.statusKey;
                this.push(res);
            } else {
                res.status = StatusKey.INVALID_ARGUMENTS;
                this.push(res);
            }
        }
    }
};
