/**
 * @fileoverview
 *
 * The file assembles the LRU cache, binary protocol and configuration modules
 * and exposes a TCP server which provides GET/SET/DELETE with cas functionality.
 */
import net = require('net');
import stream = require('stream');

import {LruCache} from './lru_cache';
import {config} from './config';
import { RequestParser, MemcacheServer } from './stream_transformer';


export function createServer(port: number = undefined, ready: Function): net.Server {
    const cache = new LruCache(config.cacheCapacity);
    const server = net.createServer((socket: net.Socket) => {
        console.log(`CLIENT CONNECTED: ${socket.remoteAddress}:${socket.remotePort}`);
        socket
            .pipe(new RequestParser())
            .pipe(new MemcacheServer(cache))
            .pipe(socket);
    });
    server.maxConnections = config.maxConnections;
    server.listen(port, () => {
        ready();
        const address = server.address();
        console.log(`Server listen on ${address.address}:${address.port}.`);

    });
    return server;
}
