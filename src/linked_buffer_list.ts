import {
    LinkedList,
    ILinkedListNode
} from './linked_list';


const MAX_CAS = 'ffffffffffffffff';


export class BufferCell implements ILinkedListNode {
    key: string;
    cas: string;
    buffer: Buffer|null;
    next: BufferCell|null;
    prev: BufferCell|null;

    constructor(key: string) {
        this.key = key;
        this.cas = '';
        this.buffer = null;
        this.next = null;
        this.prev = null;
    }
}


export class LinkedBufferList extends LinkedList {
    public capacity: number;
    public usedCapacity: number;

    constructor(capacity: number) {
        super();
        this.capacity = capacity;
        this.usedCapacity = 0;
    }

    get remainingCapacity() : number {
        return this.capacity - this.usedCapacity;
    }

    get leastCasCell(): BufferCell {
        let cur: BufferCell = <BufferCell> this.head;
        let minCasBufferCell: BufferCell = null;
        let minCas = MAX_CAS;
        while (cur) {
            if (cur.cas < minCas) {
                minCasBufferCell = cur;
                minCas = cur.cas;
            }
            cur = cur.next;
        }
        return minCasBufferCell;
    }

    // update the buffer and update the cache capacity
    updateBuffer(cell: BufferCell, buffer: Buffer) {
        const oldBufSize = cell.buffer ? cell.buffer.length : 0;
        const newBufSize = buffer.length;
        cell.buffer = buffer;
        this.usedCapacity = this.usedCapacity - oldBufSize + newBufSize;
    }

    // Recursively release storage space for bytes, returns actual bytes
    // released.
    gc(bytes: number): void {
        const cell = this.leastCasCell;
        this.delete(cell);
        const releasedBytes = cell.buffer.length;
        this.usedCapacity -= releasedBytes;
        if (releasedBytes < bytes) {
            this.gc(bytes - releasedBytes);
        }
    }
}
