/**
 * @fileoverview
 *
 * Helper functions for conversion between binary and JSON representation of a
 * request or response packet.
 */

import { MagicKey, OpKey, StatusKey,
         magicBytes, statusBytes, opBytes } from './defs';

import { buildHeader } from './helper';


interface MemcacheRequest {
    magic: MagicKey,
    optCode: OpKey,
    keyLen: number,
    extrasLen: number,
    dataType: number,
    reserved: number,
    bodyLen: number,
    opaque: number,
    cas: string,
    key: string,
    buffer?: Buffer,
}


interface MemcacheResponse {
    magic: MagicKey,
    optCode: OpKey,
    keyLen: number,
    extrasLen: number,
    dataType: number,
    status: StatusKey,
    bodyLen: number,
    opaque: number,
    cas: string,
    buffer?: Buffer,
};


/**
 * Parse a memcache request into a request header.
 *
 *  Request header:
 *  Byte/     0       |       1       |       2       |       3       |
 *     /              |               |               |               |
 *    |0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|
 *    +---------------+---------------+---------------+---------------+
 *   0| Magic         | Opcode        | Key length                    |
 *    +---------------+---------------+---------------+---------------+
 *   4| Extras length | Data type     | Reserved                      |
 *    +---------------+---------------+---------------+---------------+
 *   8| Total body length                                             |
 *    +---------------+---------------+---------------+---------------+
 *  12| Opaque                                                        |
 *    +---------------+---------------+---------------+---------------+
 *  16| CAS                                                           |
 *    |                                                               |
 *    +---------------+---------------+---------------+---------------+
 *    Total 24 bytes
 */
export const parseRequest = function(buffer: Buffer): MemcacheRequest {
    const keyLen: number = buffer.readUInt16BE(2);
    return {
        magic: magicBytes[buffer.readUInt8(0)],
        optCode: opBytes[buffer.readUInt8(1)],
        keyLen: keyLen,
        extrasLen: buffer.readUInt8(4),
        dataType: buffer.readUInt8(5),
        reserved: buffer.readUInt16BE(6),
        bodyLen: buffer.readUInt32BE(8),
        opaque: buffer.readUInt32BE(12),
        cas: buffer.toString('hex', 16, 24),
        key: buffer.toString('ascii', 24, 24 + keyLen)
    };
};


/**
 * Pack a request object to a buffer.
 * TODO: Build the data payload bytes as well.
 */
export const packRequest = function(req: MemcacheRequest): Buffer {
    const valueLen = req.bodyLen - req.keyLen - req.extrasLen;
    const header = buildHeader(
        MagicKey.REQUEST, req.optCode, null,
        req.keyLen, valueLen, req.extrasLen, req.opaque, req.cas);
    const keyBuf = Buffer.from(req.key);
    return Buffer.concat([header, keyBuf]);
};


/**
 * Parse a memcache response into a response header.
 *
 *  Response header:
 *  Byte/     0       |       1       |       2       |       3       |
 *     /              |               |               |               |
 *    |0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|
 *    +---------------+---------------+---------------+---------------+
 *   0| Magic         | Opcode        | Key length                    |
 *    +---------------+---------------+---------------+---------------+
 *   4| Extras length | Data type     | Status                        |
 *    +---------------+---------------+---------------+---------------+
 *   8| Total body length                                             |
 *    +---------------+---------------+---------------+---------------+
 *  12| Opaque                                                        |
 *    +---------------+---------------+---------------+---------------+
 *  16| CAS
 *    |                                                               |
 *    +---------------+---------------+---------------+---------------+
 *    Total 24 bytes
 *
 */
export const parseResponse = function(buffer: Buffer): MemcacheResponse {
    return {
        magic: magicBytes[buffer.readUInt8(0)],
        optCode: opBytes[buffer.readUInt8(1)],
        keyLen: buffer.readUInt16BE(2),
        extrasLen: buffer.readUInt8(4),
        dataType: buffer.readUInt8(5),
        status: statusBytes[buffer.readUInt16BE(6)],
        bodyLen: buffer.readUInt32BE(8),
        opaque: buffer.readUInt32BE(12),
        cas: buffer.toString('hex', 16, 24)
    };
};


/**
 * Pack a response object to a buffer.
 */
export const packResponse = function(res: MemcacheResponse): Buffer {
    const valueLen = res.bodyLen - res.keyLen - res.extrasLen;
    return buildHeader(
        MagicKey.RESPONSE, res.optCode, res.status,
        res.keyLen, valueLen, res.extrasLen, res.opaque, res.cas);
};
