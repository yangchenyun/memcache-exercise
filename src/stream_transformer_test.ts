import 'jasmine';
import { RequestParser, MemcacheServer } from './stream_transformer';
import { MagicKey, OpKey, StatusKey, keyToBytes } from './defs';
import {
    EMPTY_CAS,
    buildRequestHeader,
    buildResponseHeader
} from './helper';
import {LruCache} from './lru_cache';

const StreamTest = require('streamtest')['v2'];
const bufferEqual = require('buffer-equal');

describe('RequestParser Transformer', function() {
    let parser: RequestParser;

    beforeEach(function() {
        parser = new RequestParser();
    })

    it('Parses single get request command', function(done) {
        StreamTest.fromChunks([buildRequestHeader('get hello')])
            .pipe(parser)
            .pipe(StreamTest.toObjects((err: Error, objs: any) => {
                expect(err).toBeNull();
                expect(objs.length).toBe(1);
                const req = objs[0];
                expect(req).toEqual(jasmine.objectContaining(<any> {
                    magic: MagicKey.REQUEST,
                    optCode: OpKey.GET,
                    keyLen: 5,
                    extrasLen: 0,
                    dataType: 0,
                    reserved: 0,
                    bodyLen: 5,
                    opaque: 0,
                    cas: EMPTY_CAS,
                    key: 'hello'
                }));
                expect(req.buffer).toBeUndefined();
                done();
            }));
    });

    it('Parses single set request command', function(done) {
        StreamTest.fromChunks([buildRequestHeader('set hello\r\nworld')])
            .pipe(parser)
            .pipe(StreamTest.toObjects((err: Error, objs: any) => {
                expect(err).toBeNull();
                expect(objs.length).toBe(1);
                const req = objs[0];
                expect(req).toEqual(jasmine.objectContaining(<any> {
                    magic: MagicKey.REQUEST,
                    optCode: OpKey.SET,
                    keyLen: 5,
                    extrasLen: 0,
                    dataType: 0,
                    reserved: 0,
                    bodyLen: 10,
                    opaque: 0,
                    cas: EMPTY_CAS,
                    key: 'hello'
                }));
                expect(bufferEqual(req.buffer, Buffer.from('world')))
                    .toBeTruthy();
                done();
            }));

    });

    it('Parses multiple get request command', function(done) {
        StreamTest.fromChunks([
            buildRequestHeader('get hello'),
            buildRequestHeader('get world')])
            .pipe(parser)
            .pipe(StreamTest.toObjects((err: Error, objs: any) => {
                expect(err).toBeNull();
                expect(objs.length).toBe(2);
                const req1 = objs[0];
                const req2 = objs[1];
                expect(req1).toEqual(jasmine.objectContaining(<any> {
                    magic: MagicKey.REQUEST,
                    optCode: OpKey.GET,
                    keyLen: 5,
                    extrasLen: 0,
                    dataType: 0,
                    reserved: 0,
                    bodyLen: 5,
                    opaque: 0,
                    cas: EMPTY_CAS,
                    key: 'hello',
                }));
                expect(req2).toEqual(jasmine.objectContaining(<any> {
                    magic: MagicKey.REQUEST,
                    optCode: OpKey.GET,
                    keyLen: 5,
                    extrasLen: 0,
                    dataType: 0,
                    reserved: 0,
                    bodyLen: 5,
                    opaque: 0,
                    cas: EMPTY_CAS,
                    key: 'world',
                }));
                expect(req1.buffer).toBeUndefined();
                expect(req2.buffer).toBeUndefined();
                done();
            }));
    });

    it('Parses multiple set request command', function(done) {
        StreamTest.fromChunks([
            buildRequestHeader('set hello\r\nworld'),
            buildRequestHeader('set foo\r\nbar')])
            .pipe(parser)
            .pipe(StreamTest.toObjects((err: Error, objs: any) => {
                expect(err).toBeNull();
                expect(objs.length).toBe(2);
                const req1 = objs[0];
                const req2 = objs[1];
                expect(req1).toEqual(jasmine.objectContaining(<any> {
                    magic: MagicKey.REQUEST,
                    optCode: OpKey.SET,
                    keyLen: 5,
                    extrasLen: 0,
                    dataType: 0,
                    reserved: 0,
                    bodyLen: 10,
                    opaque: 0,
                    cas: EMPTY_CAS,
                    key: 'hello',
                }));
                expect(req2).toEqual(jasmine.objectContaining(<any> {
                    magic: MagicKey.REQUEST,
                    optCode: OpKey.SET,
                    keyLen: 3,
                    extrasLen: 0,
                    dataType: 0,
                    reserved: 0,
                    bodyLen: 6,
                    opaque: 0,
                    cas: EMPTY_CAS,
                    key: 'foo',
                }));
                expect(bufferEqual(req1.buffer, Buffer.from('world')))
                    .toBeTruthy();
                expect(bufferEqual(req2.buffer, Buffer.from('bar')))
                    .toBeTruthy();
                done();
            }));
    });

    it('doesn\'t push incomplete binary request', function(done) {
        let buf = buildRequestHeader('get world');
        buf = buf.slice(0, buf.length - 1);
        StreamTest.fromChunks([buildRequestHeader('get hello'), buf])
            .pipe(parser)
            .pipe(StreamTest.toObjects((err: Error, objs: any) => {
                expect(err).toBeNull();
                expect(objs.length).toBe(1);
                done();
            }));
    });
});
