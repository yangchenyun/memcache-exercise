import 'jasmine';
import {
    parseRequest,
    packRequest,
    parseResponse,
    packResponse,
} from './binary_format';

import { MagicKey, OpKey, StatusKey, keyToBytes } from './defs';
import {
    EMPTY_CAS,
    buildRequestHeader,
    buildResponseHeader
} from './helper';

const bufferEqual = require('buffer-equal');

describe('parseRequest', () => {
    it('creates packet from Buffer', () => {
        const packet = parseRequest(buildRequestHeader('get hello'));
        expect(packet).toEqual(jasmine.objectContaining((<any> {
            magic: MagicKey.REQUEST,
            optCode: OpKey.GET,
            keyLen: 5,
            extrasLen: 0,
            dataType: 0,
            reserved: 0,
            bodyLen: 5,
            opaque: 0,
            cas: EMPTY_CAS,
            key: 'hello'
        })));
    });
});

describe('packRequest', () => {
    const req = {
        magic: <MagicKey> MagicKey.REQUEST,
        optCode: <OpKey> OpKey.GET,
        keyLen: 5,
        extrasLen: 0,
        dataType: 0,
        reserved: 0,
        bodyLen: 5,
        opaque: 0,
        cas: EMPTY_CAS,
        key: 'world'
    };
    it('creates buffer from packet object', () => {
        const buf = packRequest(req);
        expect(bufferEqual(buf, buildRequestHeader('get world'))).toBeTruthy();
    });
});

describe('parseResponse', () => {
    it('creates packet from Buffer', () => {
        const packet = parseResponse(buildResponseHeader('delete foo'));
        expect(packet).toEqual(jasmine.objectContaining((<any> {
            magic: MagicKey.RESPONSE,
            optCode: OpKey.DELETE,
            keyLen: 3,
            extrasLen: 0,
            dataType: 0,
            status: StatusKey.NO_ERROR,
            bodyLen: 3,
            opaque: 0,
            cas: EMPTY_CAS
        })));
    });
});

describe('packResponse', () => {
    const res = {
            magic: <MagicKey> MagicKey.RESPONSE,
            optCode: <OpKey> OpKey.DELETE,
            keyLen: 3,
            extrasLen: 0,
            dataType: 0,
            status: <StatusKey> StatusKey.NO_ERROR,
            bodyLen: 3,
            opaque: 0,
            cas: EMPTY_CAS
    };

    it('creates buffer from packet object', () => {
        const buf = packResponse(res);
        expect(bufferEqual(buf, buildResponseHeader('delete foo'))).toBeTruthy();
    });

    it('creates buffer with correct status', () => {
        const buf = packResponse(
            Object.assign({}, res, {status: StatusKey.INCR_DECR_ON_NON_NUMERIC})
        );
        expect(bufferEqual(buf, buildResponseHeader(
            'delete foo', StatusKey.INCR_DECR_ON_NON_NUMERIC
        ))).toBeTruthy();
    });
});
