const assert = require('assert');

export interface ILinkedListNode {
    key: string;
    next: ILinkedListNode|null;
    prev: ILinkedListNode|null;
}

export class LinkedList {
    public size: number = 0;
    public head: ILinkedListNode = null;
    public tail: ILinkedListNode = null;

    search(key: string): ILinkedListNode|null {
        let node: ILinkedListNode = this.head;
        while (node) {
            if (node.key == key) {
                return node;
            }
            node = node.next;
        }
        return null;
    }

    /**
     * Adds node to the head of list.
     */
    add(key: string, node: ILinkedListNode): void {
        node.next = this.head;
        if(this.head != null) { this.head.prev = node; }
        this.head = node;
        if(this.tail == null) { this.tail = node; }
        this.size++;
    }

    /**
     * Deletes node to the head of list.
     */
    delete(node: ILinkedListNode): void {
        if (node == this.tail && node == this.head) {
            this.tail = this.head = null;
            this.destroy(node);
            this.size--;
            assert(this.size == 0);
            return;
        }

        if (node == this.tail) {
            this.tail = node.prev;
            this.tail.next = null;
            this.destroy(node);
            this.size--;
            return;
        }

        if (node == this.head) {
            this.head = this.head.next;
            this.head.prev = null;
            this.destroy(node);
            this.size--;
            return;
        }

        node.prev.next = node.next;
        node.next.prev = node.prev;
        this.destroy(node);
        this.size--;
        return;
    }

    destroy(node: ILinkedListNode) {
        node.next = null;
        node.prev = null;
        delete node.key;
    }
}
