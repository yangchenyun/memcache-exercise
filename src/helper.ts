/**
 * @fileoverview
 *
 * Helpers to construct a binary request/response commands from text string.
 * This DOES NOT implement the text protocol. The function is mainly used for
 * testing.
 *
 * <command> <key>\r\n<payload>\r\n
 */

import { MagicKey, OpKey, StatusKey, keyToBytes } from './defs';

const HEADER_LEN = 24;

export const EMPTY_CAS = '0000000000000000';  // NOTE: 8 bytes encoded in hex

export const buildRequestHeader = function(text: string, cas=EMPTY_CAS): Buffer {
    let totalLen = 0;
    let extraLen = 0;
    let [textLines, data, _] = text.split('\r\n');
    if (!data) data = '';
    let [command, key] = textLines.split(' ');
    command = command.toUpperCase();
    const header = buildHeader(MagicKey.REQUEST, <OpKey> command, null,
                               key.length, data.length, 0, 0, cas)
    const keyBuf = Buffer.from(key);
    const valueBuf = data != '' ? Buffer.from(data) : Buffer.alloc(0);
    return Buffer.concat([header, keyBuf, valueBuf]);
}

export const buildResponseHeader = function(
    text: string, status: StatusKey=StatusKey.NO_ERROR, cas=EMPTY_CAS): Buffer {
    const [textLines, data, _] = text.split('\r\n');
    let [command, key] = textLines.split(' ');
    command = command.toUpperCase();
    return buildHeader(MagicKey.RESPONSE, <OpKey> command, status,
                       key.length, 0, 0, 0, cas)
}

export function buildHeader(magic: MagicKey,
                     op: OpKey,
                     status: StatusKey | null,
                     keyLen: number,
                     valueLen: number,
                     extraLen: number,
                     opaque: number,
                     cas: string): Buffer {
    let totalLen = keyLen + extraLen + valueLen;
    const buf = new Buffer(HEADER_LEN);
    buf.writeUInt8(keyToBytes[magic] , 0);
    buf.writeUInt8(keyToBytes[op], 1);
    buf.writeUInt16BE(keyLen, 2);
    buf.writeUInt8(extraLen, 4);

    buf.writeUInt8(0, 5);
    if (magic == MagicKey.RESPONSE) {
        buf.writeUInt16BE(keyToBytes[status], 6);
    } else {
        buf.writeUInt16BE(0, 6);
    }
    buf.writeUInt32BE(totalLen, 8);
    buf.writeUInt32BE(0, 12);
    buf.write(cas, 16, 8, 'hex');
    return buf;
}
