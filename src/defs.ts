/**
 * @fileoverview
 *
 * Definitions of bytes used in request / response header.
 *
 * See more on '3. Defined Values' chapter of
 * https://cloud.github.com/downloads/memcached/memcached/protocol-binary.txt
 */

interface Code {
    bytes: number,
    key: MagicKey|StatusKey|OpKey,
    description?: string,
    extras?: string[],
}

function strEnum<T extends string>(o: Array<T>): {[K in T]: K} {
  return o.reduce((res, key) => {
    res[key] = key;
    return res;
  }, Object.create(null));
}

export const MagicKey = strEnum([
    'REQUEST',
    'RESPONSE',
])
export type MagicKey = keyof typeof MagicKey;

export const StatusKey = strEnum([
    'NO_ERROR',
    'KEY_NOT_FOUND',
    'KEY_EXISTS',
    'VALUE_TOO_LARGE',
    'INVALID_ARGUMENTS',
    'ITEM_NOT_STORED',
    'INCR_DECR_ON_NON_NUMERIC',
    'UNKNOWN_COMMAND',
    'OUT_OF_MEMORY',
]);
export type StatusKey = keyof typeof StatusKey;

export const OpKey = strEnum([
    'GET',
    'SET',
    'ADD',
    'REPLACE',
    'DELETE',
    'INCREMENT',
    'DECREMENT',
    'QUIT',
    'FLUSH',
    'GETQ',
    'NOOP',
    'VERSION',
    'GETK',
    'GETKQ',
    'APPEND',
    'PREPEND',
    'STAT',
    'SETQ',
    'ADDQ',
    'REPLACEQ',
    'DELETEQ',
    'INCREMENTQ',
    'DECREMENTQ',
    'QUITQ',
    'FLUSHQ',
    'APPENDQ',
    'PREPENDQ',
]);
export type OpKey = keyof typeof OpKey;

const magicCodes: Code[] = [
    { bytes:0x80, key: MagicKey.REQUEST },
    { bytes:0x81, key: MagicKey.RESPONSE },
];

const statusCodes: Code[] = [
  { bytes: 0x0000, key: StatusKey.NO_ERROR, description: "No Error" },
  { bytes: 0x0001, key: StatusKey.KEY_NOT_FOUND, description: "Key not found" },
  { bytes: 0x0002, key: StatusKey.KEY_EXISTS, description: "Key exists" },
  { bytes: 0x0003, key: StatusKey.VALUE_TOO_LARGE, description: "Value too large" },
  { bytes: 0x0004, key: StatusKey.INVALID_ARGUMENTS, description: "Invalid arguments" },
  { bytes: 0x0005, key: StatusKey.ITEM_NOT_STORED, description: "Item not stored" },
  { bytes: 0x0006, key: StatusKey.INCR_DECR_ON_NON_NUMERIC, description: "Incr/Decr on non-numeric value" },
  { bytes: 0x0081, key: StatusKey.UNKNOWN_COMMAND, description: "Unknown command" },
  { bytes: 0x0082, key: StatusKey.OUT_OF_MEMORY, description: "Out of memory" },
];

const opCodes: Code[] = [
    { bytes: 0x00, key: OpKey.GET },
    { bytes: 0x01, key: OpKey.SET },
    { bytes: 0x02, key: OpKey.ADD },

    { bytes: 0x03, key: OpKey.REPLACE },
    { bytes: 0x04, key: OpKey.DELETE },
    { bytes: 0x05, key: OpKey.INCREMENT },
    { bytes: 0x06, key: OpKey.DECREMENT },
    { bytes: 0x07, key: OpKey.QUIT },
    { bytes: 0x08, key: OpKey.FLUSH },
    { bytes: 0x09, key: OpKey.GETQ },
    { bytes: 0x0A, key: OpKey.NOOP },

    { bytes: 0x0B, key: OpKey.VERSION },
    { bytes: 0x0C, key: OpKey.GETK },
    { bytes: 0x0D, key: OpKey.GETKQ },
    { bytes: 0x0E, key: OpKey.APPEND },
    { bytes: 0x0F, key: OpKey.PREPEND },
    { bytes: 0x10, key: OpKey.STAT },
    { bytes: 0x11, key: OpKey.SETQ },
    { bytes: 0x12, key: OpKey.ADDQ },
    { bytes: 0x13, key: OpKey.REPLACEQ },
    { bytes: 0x14, key: OpKey.DELETEQ },
    { bytes: 0x15, key: OpKey.INCREMENTQ },
    { bytes: 0x16, key: OpKey.DECREMENTQ },
    { bytes: 0x17, key: OpKey.QUITQ },
    { bytes: 0x18, key: OpKey.FLUSHQ },
    { bytes: 0x19, key: OpKey.APPENDQ },
    { bytes: 0x1A, key: OpKey.PREPENDQ },
];

export const magicBytes: { [key: number]: MagicKey } = {};
magicCodes.forEach((code) => {
    magicBytes[code.bytes] = <MagicKey> code.key;
})

export const statusBytes: { [key: number]: StatusKey } = {};
statusCodes.forEach((code) => {
    statusBytes[code.bytes] = <StatusKey> code.key;
})

export const opBytes: { [key: number]: OpKey } = {};
opCodes.forEach((code) => {
    opBytes[code.bytes] = <OpKey> code.key;
})

export const keyToBytes: { [key: string]: number } = {};

magicCodes.forEach((code) => {
    keyToBytes[code.key] = code.bytes;
})

statusCodes.forEach((code) => {
    keyToBytes[code.key] = code.bytes;
})

opCodes.forEach((code) => {
    keyToBytes[code.key] = code.bytes;
})
