export const config = {
    maxConnections: 100,
    cacheCapacity: 1024 * 1024
};
