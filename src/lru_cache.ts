/**
 * @fileoverview
 *
 * LRU cache implementation supports:
 *
 * - Configurable resource constrains.
 * - Check And Set(cas) operation.
 *
 * Use the cas bit as the age bit as well, successful get/set operation should
 * advance the cas; items are sorted by cas.
 *
 */
import { StatusKey } from './defs';
import { LinkedBufferList, BufferCell } from './linked_buffer_list';

const NULL_BUF = Buffer.alloc(0);

export class CacheError extends Error {
    statusKey: StatusKey;

    constructor(key: StatusKey) {
        super();
        this.statusKey = key;
    }
}

export class LruCache {
    private ageBit: number;  // 64 bit, used to count operations
    private store: LinkedBufferList;

    constructor(capacity: number) {
        this.ageBit = 0;
        this.store = new LinkedBufferList(capacity)
    }

    /**
     * Get the stored value by key.
     */
    get(key: string): BufferCell|null {
        const cell = <BufferCell> this.store.search(key);
        this.checkCellNotNull_(cell);
        this.saveAndforwardAgeBit_(cell);
        return cell;
    }

    /**
     * Set or override the stored buffer by key.
     */
    set(key: string, buffer: Buffer): void {
        const bufSize = buffer.length;
        if (bufSize > this.store.capacity) {
            throw new CacheError(StatusKey.VALUE_TOO_LARGE);
        }
        if (bufSize > this.store.remainingCapacity) {
            this.store.gc(bufSize - this.store.remainingCapacity);
        }

        let cell = <BufferCell> this.store.search(key);
        if (!cell) {
            cell = new BufferCell(key);
            this.store.add(key, cell);
        }
        this.store.updateBuffer(cell, buffer);
        this.saveAndforwardAgeBit_(cell);
    }

    /**
     * Check and set.
     */
    cas(key: string, buffer: Buffer, cas: string): void {
        const cell = <BufferCell> this.store.search(key);
        this.checkCellNotNull_(cell);
        if (cell.cas != cas) {
            throw new CacheError(StatusKey.ITEM_NOT_STORED);
        }
        this.set(key, buffer);
    }

    /**
     * Delete the stored value by key.
     */
    delete(key: string): void {
        const cell = <BufferCell> this.store.search(key);
        this.checkCellNotNull_(cell);
        this.store.updateBuffer(cell, NULL_BUF);
        this.store.delete(cell);
    }

    checkCellNotNull_(cell: BufferCell) {
        if (!cell) {
            throw new CacheError(StatusKey.KEY_NOT_FOUND);
        }
    }

    // forward the agebit and save them to cell
    // @private
    saveAndforwardAgeBit_(cell: BufferCell): void {
        this.ageBit++;
        cell.cas = this.ageBit.toString(16);
    }
};
