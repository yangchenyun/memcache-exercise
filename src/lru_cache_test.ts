import 'jasmine';
import {
    LruCache,
    CacheError
} from './lru_cache';

import { MagicKey, OpKey, StatusKey, keyToBytes } from './defs';

const bufferEqual = require('buffer-equal');


describe('LruCache Functionality', () => {
    let cache: LruCache;
    const LIMIT = 1024;
    describe('#get', () => {
        beforeEach(() => {
            cache = new LruCache(LIMIT);
        })

        it('raises error when key not found', () => {
            expect(() => cache.get('a'))
                .toThrow(new CacheError(StatusKey.KEY_NOT_FOUND));
        })

        it('returns buffer if found', () => {
            const buf = Buffer.from('world');
            cache.set('hello', buf);
            expect(bufferEqual(cache.get('hello').buffer, buf)).toBeTruthy();
        })
    })

    describe('#set', () => {
        beforeEach(() => {
            cache = new LruCache(LIMIT);
        })

        it('saves buffer <= capacity', () => {
            const buf = Buffer.alloc(LIMIT, 'a');
            cache.set('hello', buf);
            expect(bufferEqual(cache.get('hello').buffer, buf)).toBeTruthy();
        })

        it('saves multiple buffers if total size <= capacity', () => {
            const buf1 = Buffer.alloc(LIMIT/2, 'a');
            const buf2 = Buffer.alloc(LIMIT/2, 'b');
            cache.set('a', buf1);
            cache.set('b', buf2);
            expect(bufferEqual(cache.get('a').buffer, buf1)).toBeTruthy();
            expect(bufferEqual(cache.get('b').buffer, buf2)).toBeTruthy();
        });

        it('overrides previous values on same key', () => {
            const buf1 = Buffer.alloc(LIMIT/2, 'a');
            const buf2 = Buffer.alloc(LIMIT/2, 'b');
            cache.set('a', buf1);
            cache.set('a', buf2);
            expect(bufferEqual(cache.get('a').buffer, buf2)).toBeTruthy();
        });

        it('#set should generate cas token if null.', () => {
            const buf1 = Buffer.alloc(8, 'a');
            cache.set('hello', buf1);
            expect(cache.get('hello').cas).not.toEqual(null)
        })

        it('raises error when saving buffer > capacity', () => {
            const buf = Buffer.alloc(LIMIT + 1, 'a');
            expect(() => cache.set('hello', buf))
                .toThrow(new CacheError(StatusKey.VALUE_TOO_LARGE));
        })
    })

    describe('#delete', () => {
        beforeEach(() => {
            cache = new LruCache(LIMIT);
        })

        it('raises error when key not found', () => {
            expect(() => cache.delete('a'))
                .toThrow(new CacheError(StatusKey.KEY_NOT_FOUND));
        })

        it('removes buffer saved by key.', () => {
            const buf1 = Buffer.alloc(LIMIT/2, 'a');
            cache.set('a', buf1);
            cache.delete('a');
            expect(() => cache.get('a'))
                .toThrow(new CacheError(StatusKey.KEY_NOT_FOUND));
        })

        it('keeps the rest buffers', () => {
            const buf1 = Buffer.alloc(LIMIT/2, 'a');
            const buf2 = Buffer.alloc(LIMIT/2, 'b');
            cache.set('a', buf1);
            cache.set('b', buf2);
            cache.delete('a');
            expect(() => cache.get('a'))
                .toThrow(new CacheError(StatusKey.KEY_NOT_FOUND));
            expect(bufferEqual(cache.get('b').buffer, buf2)).toBeTruthy();
        })
    });

    describe('#cas', () => {
        beforeEach(() => {
            cache = new LruCache(LIMIT);
            cache.set('a', Buffer.alloc(8, 'a'));
        })

        it('raises error if key doesn\'t exist', () => {
            const buf = Buffer.alloc(0);
            const cas = cache.get('a').cas;
            expect(() => cache.cas('b', buf, cas))
                .toThrow(new CacheError(StatusKey.KEY_NOT_FOUND));
        })

        it('raises error if cas doesn\'t match', () => {
            const buf = Buffer.alloc(0);
            const cas = cache.get('a').cas;
            expect(() => cache.cas('a', buf, '0x0000000000ff'))
                .toThrow(new CacheError(StatusKey.ITEM_NOT_STORED));
        })

        it('updates the value if cas matches', () => {
            const buf = Buffer.alloc(8, 'b');
            const cas = cache.get('a').cas;
            cache.cas('a', buf, cas);
            expect(bufferEqual(cache.get('a').buffer, buf)).toBeTruthy();
        })
    });

    describe('LRU algorithm', () => {
        beforeEach(() => {
            cache = new LruCache(LIMIT);
            cache.set('a', Buffer.alloc(LIMIT/2, 'a'));
            cache.set('b', Buffer.alloc(LIMIT/2, 'b'));
        })

        it('Discards the earliest stored data', () => {
            const buf = Buffer.alloc(LIMIT/2, 'c');
            cache.set('c', buf);  // should trigger gc
            expect(() => cache.get('a'))
                .toThrow(new CacheError(StatusKey.KEY_NOT_FOUND));
            expect(cache.get('b')).not.toEqual(null);
            expect(bufferEqual(cache.get('c').buffer, buf)).toBeTruthy();
        })

        it('`set` operation will reset the age', () => {
            const buf = Buffer.alloc(LIMIT/2, 'c');

            cache.set('a', buf); // extra set operation
            cache.set('c', buf);

            expect(bufferEqual(cache.get('a').buffer, buf)).toBeTruthy();
            expect(() => cache.get('b'))
                .toThrow(new CacheError(StatusKey.KEY_NOT_FOUND));
            expect(bufferEqual(cache.get('c').buffer, buf)).toBeTruthy();
        })

        it('`get` operation will reset the age', () => {
            const buf = Buffer.alloc(LIMIT/2, 'c');

            cache.get('a'); // extra get operation
            cache.set('c', buf);

            expect(cache.get('a')).not.toEqual(null);
            expect(() => cache.get('b'))
                .toThrow(new CacheError(StatusKey.KEY_NOT_FOUND));
            expect(bufferEqual(cache.get('c').buffer, buf)).toBeTruthy();
        })

        it('Discards multiple cells until there is enough room.', () => {
            const buf = Buffer.alloc(LIMIT, 'c');
            cache.set('c', buf);

            expect(() => cache.get('a'))
                .toThrow(new CacheError(StatusKey.KEY_NOT_FOUND));
            expect(() => cache.get('b'))
                .toThrow(new CacheError(StatusKey.KEY_NOT_FOUND));
            expect(bufferEqual(cache.get('c').buffer, buf)).toBeTruthy();
        })
    })
});
