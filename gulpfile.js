const gulp = require("gulp");
const gulpIf = require("gulp-if");
const ts = require("gulp-typescript");
const tsProject = ts.createProject("tsconfig.json");
var jasmine = require('gulp-jasmine-node');

gulp.task("compile", () => {
    return gulp.src(['src/*.ts', 'src/*.js'])
        .pipe(tsProject())
        .js
        .pipe(gulp.dest("dist"));
});

gulp.task("test", ['compile'], () => {
    return gulp.src(['src/*.ts'])
        .pipe(tsProject())
        .pipe(gulp.dest("dist"))
        .pipe(gulpIf('*_test.js', jasmine()));
});

gulp.task('watch', ['compile'], () => {
    gulp.watch(['src/*.ts'], ['test']);
});
